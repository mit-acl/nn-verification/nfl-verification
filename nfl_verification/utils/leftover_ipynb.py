import numpy as np
import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)
import matplotlib.pyplot as plt
import matplotlib.patches as ptch

def leaky_relu(z):
    output = np.zeros_like(z)
    if isinstance(z, np.ndarray):
        pos_inds = np.where(z>=0)
        neg_inds = np.where(z<0)
        output[pos_inds] = z[pos_inds]
        output[neg_inds] = 0.5*z[neg_inds]
        return output
    else:
        if z >= 0:
            return z
        else:
            return 0.5*z

def sigmoid(z):
    return 1./(1+np.exp(-z))

def tanh(z):
    return np.tanh(z)

def forward(x, Ws, activation=leaky_relu):
    y = x
    for W in Ws:
#         print('y:', y)
        z = np.dot(W, y)
#         print('z:', z)
        y = activation(z)
#         print('--')
    return y

x = [0.5, 0.4]
W0 = np.eye(2)
W1 = np.eye(2)
W1[0,0] = 2; W1[1,1] = 2
Ws = [W0, W1]
forward(x, Ws)

def forward_interval(xs, Ws, activation=leaky_relu, verbose=False):
    if verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
    ys = xs
    zs = np.zeros_like(ys)
    for W in Ws:
        logger.debug('ys: {}'.format(ys))
        for i, W_row in enumerate(W):
            logger.debug('W_row: {}'.format(W_row))
            y_low = np.zeros_like(ys[0])
            y_high = np.zeros_like(ys[0])
            pos_inds = np.where(W_row >= 0)
            neg_inds = np.where(W_row < 0)
            logger.debug('pos_inds: {}, neg_inds: {}'.format(pos_inds, neg_inds))
            y_low[pos_inds] = ys[0][pos_inds]
            y_low[neg_inds] = ys[1][neg_inds]
            y_high[pos_inds] = ys[1][pos_inds]
            y_high[neg_inds] = ys[0][neg_inds]
            logger.debug('y_low: {}, y_high: {}'.format(y_low, y_high))
            zs[0,i] = np.dot(W_row, y_low)
            zs[1,i] = np.dot(W_row, y_high)
            logger.debug('zs[0,i]: {}, zs[1,i]: {}'.format(zs[0,i], zs[1,i]))
#         for i, y in enumerate(ys):
#             logger.debug("W: {}".format(W))
#             logger.debug("y: {}".format(y))
#             zs[i] = np.matmul(W, y)
        logger.debug('zs (unordered): {}'.format(zs))
        zs = np.array([np.min(zs, axis=0), np.max(zs, axis=0)])
        logger.debug("zs: {}".format(zs))
        ys = activation(zs)
        logger.debug("ys: {}".format(ys))
        logger.debug('--')
    return ys

xs = np.array([[0.5, 0.1], [0.6, 0.3]])
W0 = np.eye(2)
W1 = np.eye(2)
W0[0,1] = -1
W1[0,0] = 2; W1[1,1] = 2
Ws = [W0, W1]
forward_interval(xs, Ws, verbose=True)

xs = np.array([[0.5, -0.8], [0.6, 0.3]])
Ws = [np.random.uniform(size=(2,2)) for _ in range(3)]
activation = tanh
# W0 = np.eye(2)
# W1 = np.array([[0.7, -0.3],[-0.1, 0.1]])
# Ws = [W0, W1, W1, W1]
# Ws = [W0, W0, W0]

num_samples = 1000
input_pts = np.random.uniform(low=xs[0], high=xs[1], size=(num_samples,)+xs[0].shape)
output_pts = np.array([forward(x, Ws, activation=activation) for x in input_pts])
lower_bnds = np.min(output_pts, axis=0)
upper_bnds = np.max(output_pts, axis=0)
print("Lower Bnds (Samp):", lower_bnds)
print("Upper Bnds (Samp):", upper_bnds)
bnds_ibp = forward_interval(xs, Ws, activation=activation, verbose=False)
print("Lower Bnds (IBP):", bnds_ibp[0])
print("Upper Bnds (IBP):", bnds_ibp[1])

assert(np.all(bnds_ibp[0,:] < lower_bnds))
assert(np.all(bnds_ibp[1,:] > upper_bnds))

plt.scatter(output_pts[:,0], output_pts[:,1])
print(bnds_ibp[0,:], bnds_ibp[1,0]-bnds_ibp[0,0], bnds_ibp[1,1]-bnds_ibp[0,1])
rect = ptch.Rectangle(bnds_ibp[0,:], bnds_ibp[1,0]-bnds_ibp[0,0], bnds_ibp[1,1]-bnds_ibp[0,1],
                     fc='None', ec='tab:red')
plt.gca().add_patch(rect)
plt.show()

output_range = np.array([[0.1, -0.8], [0.6, 0.3]])
layer_sizes = [2, 2, 5, 2, 2]
Ws = []
for i in range(len(layer_sizes)-1):
    Ws.append(np.random.uniform(low=-1, high=1, size=(layer_sizes[i+1],layer_sizes[i])))
# Ws = [np.eye(2)]
# Ws = [np.random.uniform(size=(2,2)) for _ in range(3)]
activation = tanh
xs = np.array([[-2, -2], [2, 2]])

num_samples = 1000
input_pts = np.random.uniform(low=xs[0], high=xs[1], size=(num_samples,)+xs[0].shape)
output_pts = np.array([forward(x, Ws, activation=activation) for x in input_pts])
print(output_pts)
mask = np.logical_and.reduce((
    output_pts[:,0] > output_range[0,0], 
    output_pts[:,0] < output_range[1,0], 
    output_pts[:,1] > output_range[0,1],
    output_pts[:,1] < output_range[1,1],
))
good_inds = np.where(mask)
bad_inds = np.logical_not(mask)

fig, axes = plt.subplots(1,2)

axes[1].scatter(output_pts[good_inds,0], output_pts[good_inds,1], color='tab:green')
axes[1].scatter(output_pts[bad_inds,0], output_pts[bad_inds,1], color='tab:red')
rect = ptch.Rectangle(output_range[0,:], output_range[1,0]-output_range[0,0], output_range[1,1]-output_range[0,1],
                     fc='None', ec='tab:red')
axes[1].add_patch(rect)

axes[0].scatter(input_pts[good_inds,0], input_pts[good_inds,1], color='tab:green')
axes[0].scatter(input_pts[bad_inds,0], input_pts[bad_inds,1], color='tab:red')

plt.show()


# lower_bnds = np.min(output_pts, axis=0)
# upper_bnds = np.max(output_pts, axis=0)
# print("Lower Bnds (Samp):", lower_bnds)
# print("Upper Bnds (Samp):", upper_bnds)
# bnds_ibp = forward_interval(xs, Ws, activation=activation, verbose=False)
# print("Lower Bnds (IBP):", bnds_ibp[0])
# print("Upper Bnds (IBP):", bnds_ibp[1])

def leaky_relu_inv(z):
    output = np.zeros_like(z)
    pos_inds = np.where(z>=0)
    neg_inds = np.where(z<0)
    output[pos_inds] = z[pos_inds]
    output[neg_inds] = 2.*z[neg_inds]
    return output

def sigmoid_inv(z):
    return np.log(z / (1-z))

def tanh_inv(z):
    return np.arctanh(z)

def inverted_nn(y, Ws, activation=leaky_relu):
    x = y
    for W in Ws[::-1]:
        z = globals()[activation.__name__+'_inv'](x)
        m,n = W.shape
        if m >= n:
            WL = np.dot(np.linalg.inv(np.dot(W.T, W)), W.T)
            x = np.dot(WL, z)
        else:
            WR = np.dot(W.T, np.linalg.inv(np.dot(W, W.T)))
            x = np.dot(WR, z)
    return x

output_range = np.array([[0.1, -0.8, 0.5, 0.6], [0.6, 0.9, 0.7, 1.2]])
# Ws = [np.eye(2)]

layer_sizes = [2, 3, 4]
Ws = []
for i in range(len(layer_sizes)-1):
    Ws.append(np.random.uniform(low=-1, high=1, size=(layer_sizes[i+1],layer_sizes[i])))

# Ws = [np.random.uniform(low=-1, high=1, size=(2,2)) for _ in range(3)]
activation = leaky_relu
xs = np.tile(np.array([-2,2]), (layer_sizes[0],1)).T

num_samples = 1000
input_pts = np.random.uniform(low=xs[0], high=xs[1], size=(num_samples,)+xs[0].shape)
output_pts = np.array([forward(x, Ws, activation=activation) for x in input_pts])

input_pts_est = np.array([inverted_nn(output_pt, Ws, activation=activation) for output_pt in output_pts])

fig, axes = plt.subplots(1,2)
axes[0].scatter(input_pts[:,0], input_pts[:,1], color='tab:green')
axes[0].set_xlabel('True Inputs')
axes[1].scatter(input_pts_est[:,0], input_pts_est[:,1], color='tab:blue')
axes[1].set_xlabel('Est. Inputs')
plt.show()


# Idea: Write a fn that inverts the NN and then you can use auto-lirpa
# - [ ] check that leaky-relu is implemented in autolirpa
# - [ ] write fn to invert NN (check that single pts work)
# - [ ] write fn that samples a bunch of input pts to and colors them based on ones whose outputs lie in set
# - [ ] load that into auto-lirpa and see if output makes sense

w = np.random.uniform(size=(50,30))

w

w_inv = np.linalg.pinv(w)

w_inv@w

sig = sigmoid(np.array([0.1, 0.2, 0.3]))
sigmoid_inv(sig)

x = np.array([0.1, 0.5])
layer_sizes = [2, 5, 2]
Ws = []
for i in range(len(layer_sizes)-1):
    Ws.append(np.random.uniform(low=-1, high=1, size=(layer_sizes[i+1],layer_sizes[i])))
activation = leaky_relu
output = forward(x, Ws, activation=activation)
inp = inverted_nn(output, Ws, activation=activation)
print(inp)

Ws

np.dot(Ws[1], np.dot(np.linalg.pinv(Ws[1]), Ws[1]))

np.linalg.pinv(Ws[1])

Ws[1]

W = Ws[0]
WL = np.dot(np.linalg.inv(np.dot(W.T, W)), W.T)

Ws

np.dot(WL, W)

W = Ws[1]
WR = np.dot(W.T, np.linalg.inv(np.dot(W, W.T)))

np.dot(W, WR)

np.tile(np.array([-2,2]), (3,1)).T

import scipy.io
mat = scipy.io.loadmat('/Users/mfe/Downloads/build.mat')

mat

