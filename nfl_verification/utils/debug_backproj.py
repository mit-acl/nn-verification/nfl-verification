import numpy as np
import matplotlib
matplotlib.use('MacOSX')
import matplotlib.pyplot as plt

import torch

from nfl_verification.propagators.ClosedLoopCROWNIBPCodebasePropagator import ClosedLoopCROWNPropagator
from nfl_verification.dynamics import DoubleIntegrator, GroundRobotSI
from nfl_verification.utils.nn import load_controller

from auto_LiRPA import BoundedModule, BoundedTensor, PerturbationLpNorm


c = ClosedLoopCROWNPropagator()
c.dynamics = GroundRobotSI()

controller = load_controller(system="GroundRobotSI", model_name="avoid_origin_controller_simple")

my_input = torch.empty((1,2))
# Wrap the model with auto_LiRPA
model = BoundedModule(controller, my_input)

from crown_ibp.bound_layers import BoundSequential
torch_model_ = BoundSequential.convert(controller, {"same-slope": False})

c.network = controller

num_control_inputs = 2
C = torch.eye(num_control_inputs).unsqueeze(0)

input_ranges = [
    np.array([[-6.4375, 0.], [-6.28125, 0.1875]]),
    np.array([[-6.4375, 0.], [-6.28125+0.5, 2*0.1875]]),
    # np.array([[1.75, 0.859375], [1.8125, 0.9375]]),
    # np.array([[1.75, 0.78125], [1.875, 0.9375]]),
]
colors = ['red', 'blue']
upper_ls = ['-', '--']
lower_ls = ['-.', ':']
infos = []

for i in range(len(input_ranges)):

    input_range = input_ranges[i]

    x_U = torch.Tensor([input_range[1,:]])
    x_L = torch.Tensor([input_range[0,:]])

    info = {
        'x_range': input_range[:, 0],
        'y_range': input_range[:, 1],
        'color': colors[i],
        'upper_ls': upper_ls[i],
        'lower_ls': lower_ls[i],
    }

    center = (input_range[..., 1] + input_range[..., 0]) / 2.0
    radius = ((input_range[..., 1] - input_range[..., 0]) / 2.0).astype(
        np.float32
    )
    # Define perturbation
    ptb = PerturbationLpNorm(norm=np.inf, eps=radius)
    # Make the input a BoundedTensor with perturbation
    my_input = BoundedTensor(torch.Tensor([center]), ptb)
    # Forward propagation using BoundedTensor
    prediction = model(my_input)
    # Compute LiRPA bounds
    lb, ub = model.compute_bounds(method="backward")

    # # Evaluate CROWN bounds
    # out_max_crown, out_min_crown, _, _, _, _ = torch_model_.interval_range(
    #     norm=np.inf,
    #     x_U=x_U,
    #     x_L=x_L,
    #     C=C,
    # )
    out_max_crown, _, out_min_crown, _ = torch_model_.full_backward_range(
        norm=np.inf,
        x_U=x_U,
        x_L=x_L,
        upper=True,
        lower=True,
        C=C,
    )

    info['out_max_crown'] = ub.detach().numpy()[0]
    info['out_min_crown'] = lb.detach().numpy()[0]

    print(info['out_max_crown'], info['out_min_crown'])

    info['out_max_crown'] = out_max_crown.detach().numpy()[0]
    info['out_min_crown'] = out_min_crown.detach().numpy()[0]

    print(info['out_max_crown'], info['out_min_crown'])

    print('--')

    # lower_A, upper_A, lower_sum_b, upper_sum_b = c.network(
    #     method_opt=c.method_opt,
    lower_A, upper_A, lower_sum_b, upper_sum_b = c.network.full_backward_range(
        norm=np.inf,
        x_U=x_U,
        x_L=x_L,
        upper=True,
        lower=True,
        C=C,
        return_matrices=True,
    )

    # Extract numpy array from pytorch tensors
    upper_A = upper_A.detach().numpy()[0]
    lower_A = lower_A.detach().numpy()[0]
    upper_sum_b = upper_sum_b.detach().numpy()[0]
    lower_sum_b = lower_sum_b.detach().numpy()[0]

    info['upper_sum_b'] = upper_sum_b
    info['lower_sum_b'] = lower_sum_b
    info['upper_A'] = upper_A
    info['lower_A'] = lower_A

    infos.append(info)

# plt.show()

# small = {
#     'upper_sum_b': np.array([-0.33563817], dtype=np.float32),
#     'lower_sum_b': np.array([-0.33563817], dtype=np.float32),
#     'upper_A': np.array([[-0.16435814, -0.23933075]], dtype=np.float32),
#     'lower_A': np.array([[-0.16435814, -0.23933075]], dtype=np.float32),
#     'x_range': [1.75, 1.8125],
#     'y_range': [0.859375, 0.9375],
#     'color': 'red',
# }

# big = {
#     'upper_sum_b': np.array([-0.33563817], dtype=np.float32),
#     'lower_sum_b': np.array([-0.3545304], dtype=np.float32),
#     'upper_A': np.array([[-0.16435814, -0.23933075]], dtype=np.float32),
#     'lower_A': np.array([[-0.17194238, -0.20502177]], dtype=np.float32),
#     'x_range': [1.75, 1.875],
#     'y_range': [0.78125, 0.9375],
#     'color': 'blue',
# }

# for d in [big, small]:
for d in infos:

    u_index = 0
    x_index = 0

    x = np.linspace([d['x_range'][0], 0.1], [d['x_range'][1], 0.1], 100)

    # us = c.forward_pass(x)
    us = torch_model_.forward(torch.Tensor(x)).detach().numpy()

    plt.plot(x[:, x_index], us[:, u_index], 'x', c=d['color'])

    if 'out_max_crown' in d:
        plt.axhline(d['out_max_crown'][u_index], ls=d['upper_ls'], c=d['color'])
        plt.axhline(d['out_min_crown'][u_index], ls=d['lower_ls'], c=d['color'])

    # if 'upper_A' in d:
    #     upper_bnd = (d['upper_A']@x.T).T+d['upper_sum_b']
    #     lower_bnd = (d['lower_A']@x.T).T+d['lower_sum_b']
    #     plt.plot(x[:, x_index], upper_bnd[:, u_index], d['upper_ls'], c=d['color'])
    #     plt.plot(x[:, x_index], lower_bnd[:, u_index], d['lower_ls'], c=d['color'])

plt.show()